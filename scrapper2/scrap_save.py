import pymongo
from pymongo import MongoClient
import time
from news_articles import aggregate_all_articles
from newspaper import Article
from urllib.parse import urlparse
import json
import numpy as np

#Databse intialization with unique index url
def create_mongodb(databasename, collection_name):
    #Creating a pymongo client
    client = MongoClient("mongodb://user2:user2password@139.99.45.55/")#MongoClient('localhost', 27017)

    #Getting the database instance
    db = client[databasename]
    print("Database created........")
    print(client.list_database_names())
    
    collection = db[collection_name]
    print("Collection created........")
    
    collection.create_index([ ("url", pymongo.ASCENDING) ], unique = True)
    return client, db, collection




# General Scrapper to scrap news articles important content, img_src, etc.
def scrap_news(article):

    url = article['url']
    source_name = article['source_name']

    # return object
    extracted_news = {
        'url': url,
        'content': None,
        'scrap_time': int(time.time()),
        'summarized_flag': 0,
        'similarity_flag':0,
        'similar_id':[],
        'source_name': source_name,
        'views_count': 0, #actions counts
        'likes_count': 0,
        'dislikes_count': 0,
        'share_count': 0,
        'laughing_count': 0,
        'surprise_count': 0,
        'confuse_count': 0,
        'straight_count': 0,
        'angry_count': 0,
        'comments': [],
        'comments_count': 0,
        }

    try:
        article = Article(url)
        article.download()
        article.parse()
    except:
        print('Problem in Downloading and Parsing for URL:', url)
        return extracted_news


    try:
        authors = article.authors
        date = article.publish_date
        content = article.text
        img_src = article.top_image
        title = article.title
        video_src = article.movies
    except:
        print('Problem in scraping the URL:', url)



    extracted_news['authors'] = authors
    extracted_news['date'] = date
    extracted_news['content'] = content
    extracted_news['img_src'] = img_src
    extracted_news['title'] = title
    extracted_news['video_src'] = video_src

    return extracted_news


# to remove subtopic which are substring of another subtopic
def remove_substring_from_list(arr):
  """
  Remove strings which are substring of another string in a list.
  """
  to_remove = set()
  for a in arr:
    for b in arr:
      if a != b and a in b:
        to_remove.add(a)
  
  final_arr = list( set(arr) - set(to_remove))

  return final_arr



#to get subtopic and topic from the url.
def get_subtopics_from_url(url):

    #get path and strip '/' and split based on
    #topics for the url given
    subtopic_url = set()
    topic_from_subtopic = set()    #topic list in which subtopic belong to.
    try:
        parsed_url = urlparse(url).path.strip('/').split('/')[:-1]
        #print(parsed_url)
        for topic in subtopics_list:
            t = topic.lower()
            for p in parsed_url:
                if t in p.lower():          # if topic exist in the the parsed url
                    subtopic_url.add(t)     # adding subtopic

        subtopic_url = remove_substring_from_list(list(subtopic_url))   #removing substring subtopics and only maintaining superstring.

        for subt in subtopic_url:
            topic_temp = subtopic_to_topic_map.get(subt)   #getting topic from subtopic mapping
            if topic_temp != None:                      #if the topic exist
                topic_from_subtopic.add(topic_temp)

    except Exception as E:
        print("Error in parsing and finding subtopics and topics.")
        print(E)

        return {
            "url": url,
            "subtopics": [],
            "topics": []
            }

    return {
            "url": url,
            "subtopics": list(subtopic_url),
            "topics": list(topic_from_subtopic)
            }


#load the subtopics list to search
with open("./content/all_subtopic.txt", "r") as fp:   # unloading
    subtopics_list = json.load(fp)


#load the subtopic_to_topics list to search
with open("./content/subtopic_to_topic.json", "r") as fo:   # unloading
    subtopic_to_topic_map = json.load(fo)

#load the image url which represent default for their news source.
with open("./content/default_image_urls.txt", "r") as fp:   # unloading
    default_image_urls = json.load(fp)

#load the topic_to_image_map list to search
with open("./content/topic_to_image_map.json", "r") as fo:   # unloading
    topic_to_image_map = json.load(fo)


# function to replace image if they contain default
def default_image_filter(extracted_news):
    if extracted_news["img_src"] in default_image_urls or extracted_news["img_src"] == None:
        new_image = set()
        topics_news = extracted_news.get("topics")
        try:
            for topic in topics_news:
                new_img_url = topic_to_image_map.get(topic)
                if new_img_url != "" and new_img_url != None:
                    new_image.add(new_img_url)

            new_image = list(new_image)
            if len(new_image) > 0:
                extracted_news["img_src"] = np.random.choice(new_image)
        except Exception as E:
            print(E)


        #print(str(extracted_news["_id"]),extracted_news["img_src"] )
        return extracted_news

    return extracted_news
            
def article_exist(url):
    """
    Check if article already exist.
    Return:
    True : Article already exist in DB.
    False: New Article.
    """        
    article_db = collection.find_one(
        {"url": url},
        {"_id":1}
        )
    
    if article_db == None:
        return False
    else:
        return True



client, db, collection = create_mongodb(databasename='news_scrap_db2', collection_name = 'news2')




#run the loop

while True:

    try:
        all_articles = aggregate_all_articles()
        print("Number of new articles scraped:",len(all_articles))

        for article in all_articles:

            #if article already exist in the DB, continue.
            if article_exist(article['url']):
                #print("continue", article['url'] )
                continue  

            extracted_news = scrap_news(article) #scrap the article content, call scrap_news()
            extracted_subtopics = get_subtopics_from_url(article['url'])

            #save the subtopics in news documents too.
            extracted_news['subtopics'] = extracted_subtopics['subtopics']
            extracted_news['topics'] = extracted_subtopics['topics']

            #default image filter to replace default images of news source
            try:
                extracted_news = default_image_filter(extracted_news)
            except Exception as E:
                print("Error in", E)
                print('Error in default_image_filter.')


            #only select the news if content exist.
            if extracted_news['content'] != None:


                try:
                    collection.insert_one(extracted_news)
                    print("Inserted", extracted_news['url'])
                    #print(extracted_news['subtopics'])
                    #print(extracted_news['topics'])
                    #print(extracted_news['img_src'])


                except Exception as E:
                    pass
                    #print(E)
                    #print('Duplicate, Already exist in DB.')


            else:
                print('Content None in URL:', extracted_news['url'])
                # delete faulty documents/articles later

    except Exception as E:
        print(E)
        print('Error in aggrgation_all_articles(). ')

    print('Sleep')
    time.sleep(600) #in seconds

