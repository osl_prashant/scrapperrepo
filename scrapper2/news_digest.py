import pymongo
from pymongo import MongoClient
import time
import schedule
import datetime

def create_mongodb(databasename='news_scrap_db2', collection_name = 'news2'):
    #Creating a pymongo client
    client = MongoClient("mongodb://user2:user2password@139.99.45.55/")#MongoClient('localhost', 27017)

    #Getting the database instance
    db = client[databasename]
    print("Database created........")
    print(client.list_database_names())
    
    collection = db[collection_name]
    print("Collection created........")
    
    collection.create_index([ ("url", pymongo.ASCENDING) ], unique = True)
    return client, db, collection

#Initialise
client, db, collection = create_mongodb(databasename='news_scrap_db2', collection_name = 'news2')


def job():
    cur_time = int(time.time())
    time_gap = 60*60*24 # 1 day for digest
    old = collection.find(
        {"similarity_flag": 1, "summarized_flag": 1, "scrap_time": {"$gte": cur_time - time_gap, "$lte":cur_time   }},
        {"_id": 1, "url": 1},
        sort = [("likes_count", -1)],
        limit = 10
        )
    old = [o for o in old]



    value = datetime.datetime.fromtimestamp(cur_time)
    print(value)

    digest_dict = {
        "digest_time": value,
        "news_id": old
    }
    db.news_digest.insert_one(digest_dict)
    #print(digest_dict)



schedule.every(1).day.at("16:29").do(job)  # Sevrver uses CEST. 08:00PM for IST is 16:29PM.

#job()
while True:
   schedule.run_pending()
   time.sleep(1)
