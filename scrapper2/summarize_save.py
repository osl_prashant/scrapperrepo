
import requests
import pymongo
from pymongo import MongoClient
import time
from collections import OrderedDict

#Databse intialization with unique index url
def create_mongodb(databasename='news_scrap_db2', collection_name = 'news2'):
    #Creating a pymongo client
    client = MongoClient("mongodb://user2:user2password@139.99.45.55/")#MongoClient('localhost', 27017)

    #Getting the database instance
    db = client[databasename]
    print("Database created........")
    print(client.list_database_names())
    
    collection = db[collection_name]
    print("Collection created........")
    
    collection.create_index([ ("url", pymongo.ASCENDING) ], unique = True)
    return client, db, collection

#Initialise
client, db, collection = create_mongodb(databasename='news_scrap_db2', collection_name = 'news2')


# summary API URI and Endpoints
uri = 'http://139.99.45.55:5000'
endpoint_home = '/home'
endpoint_primary_summary = '/primary_summary'
endpoint_secondary_summary = '/secondary_summary'
endpoint_keywords_spacy = '/keywords_spacy'
endpoint_keywords_textrank = '/keywords_textrank'


def secondary_summary(data):
    #secondary_summary
    result = None
    try:
        body = {
            "news": data['content'],
            "split": "True",
            "ratio": "None",
            "word_count": 100
            }
        r = requests.post(url = uri + endpoint_secondary_summary, json = body, timeout = 30) 
        #print(r.json())
        result = r.json()['Secondary_Summary']
    except:
        print('Error in Secondary Summary with URL', data['url'])
        result = None

    return result


def add_fullstop(summary):
    """
    Add a fullstop in the end of the summary.
    Some summary may end on other special charachters like ", (), !
    """
    if summary[-1] != '.' and summary[-1] != '!':
        summary += '.' 

    return summary

def primary_summary(data):
    result = None

    #if the sentences are too mall then they can lead to random summary.
    if len(data['content'].split(' ')) < 70:
        return result

    try:
        body = {
            "news": data['content'],
            "min_len": 70,
            "max_len":120
            }
        
        r = requests.post(url = uri + endpoint_primary_summary, json = body, timeout = 180) 
        #print(r.json())
        result = r.json()['Primary_Summary']
        result = add_fullstop(result)   #to filter if fullstop exist at the end or not.
    except:
        print('Error in Primary Summary with URL', data['url'])
        result = None


    return result
    
def keywords_spacy(data):
        #keywords_spacy
    result = None
    try:
        body = {
            "news": data['content']
            }
        
        r = requests.post(url = uri + endpoint_keywords_spacy, json = body) 
        #print(r.json())
        result = r.json()['keywords_spacy']
    except:
        print('Error in keywords_spacy with URL', data['url'] , timeout = 30)
        result = None

    return result

def keywords_textrank(data):
    #keywords_textrank
    result = None
    try:
        body = {
            "news": data['content'],
            "split":"True",
            "ratio": "None",
            "keyword_count":15,
            "scores": "True"
            }
        
        r = requests.post(url = uri + endpoint_keywords_textrank, json = body, timeout = 30) 
        #print(r.json())
        result = r.json()['keywords_gensim']
    except:
        print('Error in keywords_textrank with URL', data['url'])
        result = None

    return result
    

# reccuring keywords that may need to be ignored
keyword_to_ignore = ["indian express","livemint channel","the statesman","statesman", "livemint","the times of india", "times of india", "media services pvt ltd", "@indianexpress","the indian express", "hindu", "the hindu", "join mint channel", "mint", "rs", "rs.", "ap", "mint newsletters", "pti", "p) ltd", "getty/thinkstock", "india", "indian", "premises", "getty", "afp", "getty/thinkstock)", "getty images/thinkstock", "ani", "afp", "gettyimages" ]

def clean_keyword_spacy(keyword):

    try:
        keyword_list_final = []
        for k in keyword:
            if k[1] in ['PERSON', 'ORG', 'NORP', 'FAC', 'GPE', 'LOC', 'PRODUCT', 'EVENT', 'WORK_OF_ART', 'LAW', 'LANGUAGE']:
                if k[0] not in keyword_to_ignore and len(k[0]) > 2:
                    keyword_list_final.append(k[0])


        keyword_list_final = list(OrderedDict.fromkeys(keyword_list_final)) #inserted in order to maintain the weightage
    except Exception as E:
        print(E)
        print("Error in clean_keyword_spacy.")

    return keyword_list_final



def the_hindu_filters(data):
    #For The Hindu, if content contains this then avoid it.
    avoid_sent = "A letter from the Editor"
    if avoid_sent in data["content"]:
        return False
    
    return True

def business_standard_filters(data):
    #For Business Standard, if content contains this then avoid it.
    resp = data['content'].startswith("Dear Reader,\n\nBusiness Standard")
    
    return not resp


def miscellaneous_filters(data):
    #this function is used to apply some specail filtering to remove unwanted data from pipeline.
    #return False to avoid and True for OK.

    if data['source_name'] == 'The Hindu':
        final_resp = the_hindu_filters(data)
    elif data['source_name'] == 'Business Standard':
        final_resp = business_standard_filters(data)
    else:
        final_resp = True

    #final_resp = resp_hindu # binary operation of ands for other filters

    return final_resp




while True:
    try:
        try:
            fresh_scrapped_data = collection.find({"summarized_flag": 0})
            #print(len(fresh_scrapped_data))
            #print(fresh_scrapped_data)

        except:
            print("Error can't get fresh_scrapped_data")
            fresh_scrapped_data = []


        for data in fresh_scrapped_data:
            data['summarized_flag'] = 1            
            data['secondary_summary'] = secondary_summary(data)
            data['primary_summary'] = primary_summary(data)
            data['keywords_spacy'] = keywords_spacy(data)
            data['keywords_textrank'] = keywords_textrank(data)
            data['tags'] = clean_keyword_spacy(data['keywords_spacy'])


            try:
                if data['primary_summary'] != None and miscellaneous_filters(data):
                    collection.find_one_and_replace( {"_id" : data["_id"]},  data)
                    print("_id:",data["_id"] )
                else:
                    #collection.find_one_and_delete({"_id" : data["_id"]})
                    #make -1 flag. summary can't be generated
                    collection.find_one_and_update( {"_id" : data["_id"]}, {"$set": {"summarized_flag": -1} } )  

                    #print("Deleted.Summary None, _id:",data["_id"] )
                    print(".Summary None flagged, _id:",data["_id"] )


            except Exception as e:
                print("Error in updating the summary and keywords.")
                print(e)

    except Exception as E:
        print('Error while iterating fresh_scrapped_data.')
        print(E)
        

    #Sleep
    print('Sleeping Summarization loop for 0.5 min')
    time.sleep(30)





