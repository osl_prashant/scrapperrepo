import pymongo
from pymongo import MongoClient
import time
from urllib.parse import urlparse
import json
import requests
#from rich import print as rprint
from datetime import datetime

#Databse intialization with unique index url
def create_mongodb(databasename, collection_name):
    #Creating a pymongo client
    client = MongoClient("mongodb://user2:user2password@139.99.45.55/")#MongoClient('localhost', 27017)

    #Getting the database instance
    db = client[databasename]
    print("Database created........")
    print(client.list_database_names())
    
    collection = db[collection_name]
    print("Collection created........")
    
    collection.create_index([ ("name", pymongo.ASCENDING) ], unique = True)
    return client, db, collection

client, db, collection = create_mongodb(databasename='news_scrap_db2', collection_name = 'covid_stats')


def global_stats():
    try:
        x = requests.get('https://covid19-api.org/api/timeline')
        
        if x.status_code == 200:
            #rprint("[bold green]Global Link working.[/bold green]")
            print("Global Link working.")

            resp = x.json()
            resp.reverse()
            doc = {
                "name": "Global",
                "data": resp
            }

            collection.find_one_and_replace(
                {"name": "Global"},
                doc,
                upsert=True
            )
            print("Global Updated.")
        else:
            #rprint("[bold red]Global link not working.[/bold red]")
            print("Global link not working.")

    except Exception as e:
        # rprint("[bold red]Error in global_stats.[/bold red]")
        # rprint(f"[bold red]{e}[/bold red]")
        print("Error in global_stats.")
        print(e)

    
def india_stats():
    try:
        x = requests.get('https://thevirustracker.com/free-api?countryTimeline=IN')
        
        if x.status_code == 200:
            #rprint("[bold green]India Link working.[/bold green]")
            print("India Link working.")

            resp = x.json()

            resp = resp["timelineitems"][0]
            
            data = resp
            # for k in resp.keys():
            #     if k != "stat":                 
            #         temp = resp[k]
            #         temp["last_update"] = k #datetime.strptime(k, '%m/%d/%y')
            #         data.append(temp)

            doc = {
                "name": "India",
                "data": data
            }

            collection.find_one_and_replace(
                {"name": "India"},
                doc,
                upsert=True
            )
            print("India Updated.")
        else:
            #rprint("[bold red]India link not working.[/bold red]")
            print("India link not working.")

    except Exception as e:
        #rprint("[bold red]Error in india_stats.[/bold red]")
        #rprint(f"[bold red]{e}[/bold red]")
        print("Error in india_stats.")
        print(e)



while True:
    try:
        global_stats()
        india_stats()
    except Exception as e:
        #rprint("[red]Error. Please Check.[/red]")
        print("Error. Please Check.")

        print(e)

    #rprint("[bold yellow]Sleeping for 4 hours.[/bold yellow]")
    print("Sleeping for 4 hours.")

    time.sleep(60*60*4)