import pymongo
from pymongo import MongoClient
import time


#STOPWORDS
import nltk
nltk.download('stopwords')
nltk.download('punkt')
from nltk.corpus import stopwords
print(stopwords.words('english'))


from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize 
import re
stop_words = set(stopwords.words('english')) 





def create_mongodb(databasename='news_scrap_db2', collection_name = 'news2'):
    #Creating a pymongo client
    client = MongoClient("mongodb://user2:user2password@139.99.45.55/")#MongoClient('localhost', 27017)

    #Getting the database instance
    db = client[databasename]
    print("Database created........")
    print(client.list_database_names())
    
    collection = db[collection_name]
    print("Collection created........")
    
    collection.create_index([ ("url", pymongo.ASCENDING) ], unique = True)
    return client, db, collection

#Initialise
client, db, collection = create_mongodb(databasename='news_scrap_db2', collection_name = 'news2')



threshold = 0.45





keyword_to_ignore = ["indian express","livemint channel","the statesman","statesman", "livemint","the times of india", "times of india", "media services pvt ltd", "@indianexpress","the indian express" ]

def clean_keyword_spacy(keyword):
    keyword_list_final = []
    for k in keyword:
        if k[1] in ['PERSON', 'ORG', 'NORP', 'FAC', 'GPE', 'LOC', 'PRODUCT', 'EVENT', 'WORK_OF_ART', 'LAW', 'LANGUAGE']:
            if k[0] not in keyword_to_ignore:
                keyword_list_final.append(k[0])

    return keyword_list_final




def remove_stopwords(example_sent):  
    #example_sent = "This is a sample sentence, showing off the stop words filtration."

    example_sent = example_sent.lower()
    example_sent = re.sub(r"[^a-zA-Z0-9]+", ' ', example_sent)
    #print(example_sent)

    word_tokens = word_tokenize(example_sent) 

    #filtered_sentence = [w for w in word_tokens if not w in stop_words] 

    filtered_sentence = [] 

    for w in word_tokens: 
        if w not in stop_words : 
            filtered_sentence.append(w) 

    return filtered_sentence

def jaccard_sim(keywords_one, keywords_two):

    if len(keywords_one)< 4 or len(keywords_two)< 4:
        return 0

    A = set(keywords_one)
    B = set(keywords_two)

    intersection = A.intersection(B)
    union = A.union(B)

    try:
        similarity_score = len(intersection)/len(union)
    except:
        return 0
    return similarity_score


def compare_new_old(new, old):
    #OLD with New
    for i in new:

        #update similarity_flag
        try:
            collection.find_one_and_update(
                {"_id": i["_id"] },
                {"$set": {"similarity_flag": 1}}
                )
        except:
            print("Can't update similarity flag.")


        # KEYWORD
        keyword_spacy = i.get('keywords_spacy')

        #check if keywords attribute is not empty
        if keyword_spacy is not None:
            try:
                keyword_i = clean_keyword_spacy(keyword_spacy)
            except:
                keyword_i = []
                print('problem in clean_keyword_spacy')


        #TITLE
        title = i['title']

        #check if title attribute is not empty
        if title is not None:
            try:
                title_i = remove_stopwords(title)
            except:
                title_i = []
        


        for j in old:
            if j['_id'] != i['_id']:
                
                #KEYWORD
                keyword_spacy = j.get('keywords_spacy')
        
                #check if keywords attribute is not empty
                if keyword_spacy is not None:
                    try:
                        keyword_j = clean_keyword_spacy(keyword_spacy)
                    except:
                        keyword_j = []
                        print('problem in clean_keyword_spacy')

                #TITLE
                title = j['title']
                
                #check if title attribute is not empty
                if title is not None:
                    try:
                        title_j = remove_stopwords(title)
                    except:
                        title_j = []


                score = jaccard_sim(keyword_i, keyword_j) + jaccard_sim(title_i, title_j)



                if score > threshold:
                    try:
                        print(score, i["title"])
                        print(score, j["title"])
                        print("**"*50)
                    except Exception as E:
                        print(E)
                        print("Error in printing, new_old")

                    #i["similar_id"].append(j["_id"])
                    try:
                        collection.find_one_and_update({"_id": i["_id"] }, {"$push": {"similar_id": j["_id"]}})
                        collection.find_one_and_update({"_id": j["_id"] }, {"$push": {"similar_id": i["_id"]}})
                    except:
                        print('Updation error in new_old')
                
        
    

def compare_new_new(new):
    #NEW with New
    for i in new:

        #update similarity_flag
        try:
            collection.find_one_and_update(
                {"_id": i["_id"] },
                {"$set": {"similarity_flag": 1}}
                )
        except:
            print("Can't update similarity flag.")


        # KEYWORD
        keyword_spacy = i.get('keywords_spacy')

        #check if keywords attribute is not empty
        if keyword_spacy is not None:
            try:
                keyword_i = clean_keyword_spacy(keyword_spacy)
            except:
                keyword_i = []
                print('problem in clean_keyword_spacy')


        #TITLE
        title = i['title']

        #check if title attribute is not empty
        if title is not None:
            try:
                title_i = remove_stopwords(title)
            except:
                title_i = []
        


        for j in new:
            if j['_id'] != i['_id']:
                
                #KEYWORD
                keyword_spacy = j.get('keywords_spacy')
        
                #check if keywords attribute is not empty
                if keyword_spacy is not None:
                    try:
                        keyword_j = clean_keyword_spacy(keyword_spacy)
                    except:
                        keyword_j = []
                        print('problem in clean_keyword_spacy')

                #TITLE
                title = j['title']
                
                #check if title attribute is not empty
                if title is not None:
                    try:
                        title_j = remove_stopwords(title)
                    except:
                        title_j = []


                score = jaccard_sim(keyword_i, keyword_j) + jaccard_sim(title_i, title_j)




                if score > threshold:
                    try:
                        print(score, i["title"])
                        print(score, j["title"])
                        print("**"*50)
                    except Exception as E:
                        print(E)
                        print("Error in printing, new_new")
                    #i["similar_id"].append(j["_id"])
                    try:
                        collection.find_one_and_update({"_id": i["_id"] }, {"$push": {"similar_id": j["_id"]}})
                    except:
                        print('Updation error in new_new')
                


#SELECT data where summary is genrated but similairty is not

def get_new_data(): 
    new = collection.find(
        {"similarity_flag": 0, "summarized_flag": 1},
        {"keywords_spacy":1, "title":1}
        )
    new = [n for n in new]

    return new

#get time in second for last 36 hours. for getting old news


def get_old_data(hours):

    cur_time = int(time.time())
    time_gap = 60*60*hours # hours in seconds

    #SELECT data where both summary is genrated and similairty is generated (old)
    #max select only 2K
    old = collection.find(
        {"similarity_flag": 1, "summarized_flag": 1, "scrap_time": {"$gte": cur_time - time_gap  }},
        {"keywords_spacy":1,  "title":1},
        #sort = [("scrap_time", -1)],

        limit = 2000)
    old = [o for o in old]

    return old





while True:
    try:
        new = get_new_data()
    except:
        print('Error in getting new data.')

    try:
        old = get_old_data(hours=36)
    except:
        print('Error in getting old data.')
        
    print('New length', len(new))
    print('Old length', len(old))

    


    try:
        print('Comparing New with Old')
        compare_new_old(new, old)
    except Exception as E:
        print(E)
        print('Error in comparining new with old data.')

    try:
        print('Comparing New with New')
        compare_new_new(new)
    except Exception as E:
        print(E)
        print('Error in comparining new with new data.')


    print('Sleep for 3m')
    time.sleep(180)