from bs4 import BeautifulSoup
from urllib.request import urlopen,Request
from urllib.error import HTTPError, URLError
import time
import pymongo
import json
from pymongo import MongoClient


def load_html(url):
    try:
        hdr = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            }
        req = Request(url,headers=hdr)
        html = urlopen(req, timeout=10)
    except HTTPError as e:
        print(e)
        return None
    except URLError as e:
        print('The server could not be found!')
        return None
    else:
        print('Link Worked!')
        
    return html

def create_mongodb(databasename, collection_name):
    #Creating a pymongo client
    client = MongoClient("mongodb://user2:user2password@139.99.45.55/")#MongoClient('localhost', 27017)

    #Getting the database instance
    db = client[databasename]
    print("Database created........")
    print(client.list_database_names())
    
    collection = db[collection_name]
    print("Collection created........")
    
    collection.create_index([ ("question", pymongo.ASCENDING) ], unique = True)
    return client, db, collection

client, db, collection = create_mongodb(databasename='news_scrap_db2', collection_name = 'polls')


#Polls scrappers
def toi_polls(url):
    #Loading url and html
    html = load_html(url)

    # Extracting Links of Articles
    final_polls_list = []
    try:
        bs = BeautifulSoup(html.read(), 'html.parser')
        poll_list = bs.find_all("div", class_ = "polltablecnt")

        for pl in poll_list:
            question = pl.find("div",class_ = "polltxt1").get_text().strip()
            options = {}
            for p in pl.find_all("div",class_ = "optdesc optinl"):
                options[p.get_text().strip()] = 0
            if len(options) > 1 and question != None:
                final_polls_list.append({
                    "question": question,
                    "options": options,
                    "scrap_time": time.time(),
                    "total_actions": 0,
                    "source_name": "The Times Of India"

                })
        



    except Exception as E:
        print(E)

    final_polls_list.reverse()

    return final_polls_list

def bs_polls(url):
    #Loading url and html
    html = load_html(url)

    # Extracting Links of Articles
    final_polls_list = []
    try:
        bs = BeautifulSoup(html.read(), 'html.parser')
        poll_list = bs.find_all("div", class_ = "bs-poll")

        for pl in poll_list:
            question = pl.find("h2",class_ = "poll-txt").get_text().strip()
            options = {}
            for p in pl.find_all("li"):
                options[p.get_text().strip()] = 0

            if len(options) > 1 and question != None:
                final_polls_list.append({
                    "question": question,
                    "options": options,
                    "scrap_time": time.time(),
                    "total_actions": 0,
                    "source_name": "Business Standard"
                    
                })
            


    except Exception as E:
        print(E)

    return final_polls_list





##########
#Store

while True:
    try:
        url_polls_bs = "https://www.business-standard.com/poll/list"
        bs_polls_list = bs_polls(url_polls_bs)

        
        url_polls_toi = "https://timesofindia.indiatimes.com/home/polls"
        toi_polls_list = toi_polls(url_polls_toi)


        full_polls_list = bs_polls_list + toi_polls_list

        for p in full_polls_list:
            try:
                collection.insert_one(p)
            except Exception as E:
                print(E)
    except Exception as E:
        print(E)    

    print("sleeping")
    time.sleep(60*60*4)
