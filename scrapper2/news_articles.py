from bs4 import BeautifulSoup
from urllib.request import urlopen, Request
from urllib.error import HTTPError, URLError



def load_html(url):
    try:
        hdr = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.84 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            }
        req = Request(url,headers=hdr)
        html = urlopen(req, timeout=10)
    except HTTPError as e:
        print(e)
        return None
    except URLError as e:
        print('The server could not be found!')
        return None
    else:
        print('Link Worked!')
        
    return html

#Indian Express
def latest_news_urls_indian_express(url_latest):
    #Loading url and html
    html = load_html(url_latest)

    # Extracting Links of Articles
    try:
        bs = BeautifulSoup(html.read(), 'html.parser')

        # remove script or style or both
        for script in bs(["script"]):
            script.decompose()

        articles_list = bs.find('div',class_='leftpanel').find_all('div', class_ = 'title')
        articles_url = [
            {
                'url':a.find('a').attrs['href'], 
                'title':a.find('a').get_text(),
                'source_name': 'Indian Express'
            }
            for a in articles_list 
            ]

    except:
        print('Error in Extracting Links of Articles in Indian Express.')
        return []
    
    return articles_url


#TOI
def latest_news_urls_toi(url_latest):
    #Loading url and html
    html = load_html(url_latest)

    # Extracting Links of Articles
    try:
        bs = BeautifulSoup(html.read(), 'html.parser')

    # remove script or style or both
        for script in bs(["script"]):
            script.decompose()


        image_list = bs.find_all('a', class_ = 'w_img') #image contatins all important information

        articles_url = [ {
            'url': 'https://timesofindia.indiatimes.com' + image.attrs['href'],
            'title': image.attrs['title'],
            'source_name': 'The Times Of India'

            #'img_src': image.img.attrs['data-src']
        } for image in image_list ]
    
    except:
        print('Error in Extracting Links of Articles from Image in TOI.')
        return []

  
    return articles_url


#Mint
def latest_news_urls_mint(url_latest):
    #Loading url and html
    html = load_html(url_latest)

    # Extracting Links of Articles
    try:
        bs = BeautifulSoup(html.read(), 'html.parser')


        articles_list = bs.find_all('h2', class_ = 'headline') #image contatins all important information

        articles_url = [ {
            'url': 'https://www.livemint.com/' + article.a.attrs['href'],
            'title': article.get_text(),
            'source_name': 'livemint'


        } for article in articles_list ]
    
    except:
        print('Error in Extracting Links of Articles in mint.')
        return []

  
    return articles_url


#Statesman
def latest_news_urls_statesman(url_latest):
    #Loading url and html
    html = load_html(url_latest)


    # Extracting Links of Articles
    try:
        bs = BeautifulSoup(html.read(), 'html.parser')


        articles_list = bs.find_all('div',class_ = 'card__title')
        articles_url = [ 
                    {
                        'url':card.a.attrs['href'], 
                        'title':card.a.get_text(),
                        'source_name': 'The Statesman'
                     
                     } for card in articles_list ]
    
    except:
        print('Error in Extracting Links of Articles in Statesman')
        return []

  
    return articles_url


#The Hindu
def latest_news_urls_the_hindu(url_latest):
    #Loading url and html
    html = load_html(url_latest)

    # Extracting Links of Articles
    try:
        bs = BeautifulSoup(html.read(), 'html.parser')

        # remove script or style or both
        for script in bs(["script"]):
            script.decompose()

        articles_list = bs.find('ul',class_='latest-news').find_all('li')
        articles_url = [
            {
                'url':a.find('a').attrs['href'], 
                'title':a.find('a').get_text(),
                'source_name': 'The Hindu'
            }
            for a in articles_list 
            ]

    except:
        print('Error in Extracting Links of Articles in The Hindu.')
        return []
    
    return articles_url

#The Bridge
def latest_news_urls_the_bridge(url_latest):
    #Loading url and html
    html = load_html(url_latest)

    # Extracting Links of Articles
    try:
        bs = BeautifulSoup(html.read(), 'html.parser')

        # remove script or style or both
        for script in bs(["script"]):
            script.decompose()

        articles_list = bs.find_all('div',class_='item-details')
        articles_url = [
            {
                'url':a.find('a').attrs['href'], 
                'title':a.find('a').get_text(),
                'source_name': 'The Bridge'
            }
            for a in articles_list 
            ]

        articles_url = articles_url[:20] #only selecting top N to avoid too much scrapping
    except Exception as E:
        print(E)
        print('Error in Extracting Links of Articles in The Hindu.')
        return []
    
    return articles_url


#The Economic Times
def latest_news_urls_the_economic_times(url_latest):
    #Loading url and html
    html = load_html(url_latest)

    # Extracting Links of Articles
    try:
        bs = BeautifulSoup(html.read(), 'html.parser')

        # remove script or style or both
        for script in bs(["script"]):
            script.decompose()

        articles_list = bs.find('ul',itemscope='itemscope').find_all('li')
        articles_url = [
            {
                'url': "https://economictimes.indiatimes.com" + a.find('a').attrs['href'], 
                'title':a.find('a').get_text(),
                'source_name': 'The Economic Times'
            }
            for a in articles_list 
            ]
        articles_url = articles_url[:25] #only selecting top N to avoid too much scrapping
    except:
        print('Error in Extracting Links of Articles in The Economic Times.')
        return []
    return articles_url


#Business Standard
def latest_news_urls_business_standard(url_latest):
    #Loading url and html
    html = load_html(url_latest)

    # Extracting Links of Articles
    try:
        bs = BeautifulSoup(html.read(), 'html.parser')

        # remove script or style or both
        for script in bs(["script"]):
            script.decompose()

        articles_list = []
        for ul in bs.find_all('ul',class_='aticle-txt'):
            #print("ul", len(ul))
            for li in ul.find_all('li')[:3]:    #only selecting top 3 from each section. 
                #print("li",li.find('a').get_text())
                articles_list.append(li)

        articles_url = [
            {
                'url': "https://www.business-standard.com" + a.find('a').attrs['href'], 
                'title':a.find('a').get_text(),
                'source_name': 'Business Standard'
            }
            for a in articles_list 
            ]
        #articles_url = articles_url[:25] #only selecting top N to avoid too much scrapping
    except:
        print('Error in Extracting Links of Articles in Business Standard.')
        return []
    return articles_url

#DNA India
def latest_news_urls_dna_india(url_latest):
    #Loading url and html
    html = load_html(url_latest)

    # Extracting Links of Articles
    try:
        bs = BeautifulSoup(html.read(), 'html.parser')

        # remove script or style or both
        for script in bs(["script"]):
            script.decompose()

        articles_list = bs.find_all('h3', class_="headlines_pg")
        articles_url = [
            {
                'url': "https://www.dnaindia.com" + a.find('a').attrs['href'], 
                'title':a.find('a').get_text(),
                'source_name': 'DNA India'
            }
            for a in articles_list 
            ]
        articles_url = articles_url[:20] #only selecting top N to avoid too much scrapping
    except:
        print('Error in Extracting Links of Articles in DNA India.')
        return []
    return articles_url


#Hindustan Times
def latest_news_urls_hindustantimes(url_latest):
    #Loading url and html
    html = load_html(url_latest)

    # Extracting Links of Articles
    try:
        bs = BeautifulSoup(html.read(), 'html.parser')

        # remove script or style or both
        for script in bs(["script"]):
            script.decompose()

        #articles_list = bs.find('h2', class_ = "hdg3").find_all('li')

        articles_list = []
        for li_article in bs.find_all('h2',class_='hdg3'): 
                #print("li",li.find('a').get_text())
                articles_list.append(li_article)

        articles_url = [
            {
                'url': "https://www.hindustantimes.com" + a.find('a').attrs['href'], 
                'title': a.find('a').get_text(),
                'source_name': 'Hindustan Times'
            }
            for a in articles_list 
            ]
        #articles_url = articles_url[:20] #only selecting top N to avoid too much scrapping
    except:
        print('Error in Extracting Links of Articles in Hindustan Times.')
        return []
    return articles_url



def aggregate_all_articles():
    # latest page urls
    indian_express_url_latest = 'https://indianexpress.com/latest-news/'
    toi_url_latest = 'https://timesofindia.indiatimes.com/news'
    mint_url_latest = 'https://www.livemint.com/latest-news'
    statesman_url_latest = 'https://www.thestatesman.com/india'
    the_hindu_url_latest = "https://www.thehindu.com/latest-news/"
    the_bridge_url_latest = "https://thebridge.in/whats-latest/"
    the_economic_times_url_latest = "https://economictimes.indiatimes.com/news/latest-news"
    business_standard_url_latest = "https://www.business-standard.com/latest-news"
    dna_india_url_latest = "https://www.dnaindia.com/headlines"
    hindustantimes_url_latest = "https://www.hindustantimes.com/latest-news/"



    #call latest urls get latest articles list
    indian_express_articles = latest_news_urls_indian_express(indian_express_url_latest)
    toi_articles = latest_news_urls_toi(toi_url_latest)
    mint_articles = latest_news_urls_mint(mint_url_latest)
    statesman_articles  = latest_news_urls_statesman(statesman_url_latest)
    the_hindu_articles  = latest_news_urls_the_hindu(the_hindu_url_latest)
    the_bridge_articles = latest_news_urls_the_bridge(the_bridge_url_latest)
    the_economic_times_articles = latest_news_urls_the_economic_times(the_economic_times_url_latest)  
    business_standard_articles = latest_news_urls_business_standard(business_standard_url_latest)
    dna_india_articles = latest_news_urls_dna_india(dna_india_url_latest)
    hindustantimes_articles = latest_news_urls_hindustantimes(hindustantimes_url_latest)


    # appennd all articicles in a list
    news_latest_list = indian_express_articles + toi_articles + mint_articles + statesman_articles \
        + the_hindu_articles + the_bridge_articles + the_economic_times_articles + business_standard_articles + dna_india_articles \
            + hindustantimes_articles
    news_latest_list.reverse()  # reverse the ordering so that top url is scrapped last.

    return news_latest_list


#print(len(latest_news_urls_hindustantimes("https://www.hindustantimes.com/latest-news/")))

# aggregate_all_articles()

# print(len(aggregate_all_articles()))

#print(hindustantimes_articles)

# print(len(hindustantimes_articles))
# for pp in hindustantimes_articles:
#     print(pp['title'])
