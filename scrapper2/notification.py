from __future__ import print_function

import datetime
import schedule
import firebase_admin
from firebase_admin import credentials, firestore

cred = credentials.Certificate("./news-app-35f8a-firebase-adminsdk-szg4h-68ee1cc80b.json")
firebase_admin.initialize_app(cred)

from firebase_admin import messaging


import pymongo
from pymongo import MongoClient
import time

def create_mongodb(databasename='news_scrap_db2', collection_name = 'news2'):
    #Creating a pymongo client
    client = MongoClient("mongodb://user2:user2password@139.99.45.55/")#MongoClient('localhost', 27017)

    #Getting the database instance
    db = client[databasename]
    print("Database created........")
    print(client.list_database_names())
    
    collection = db[collection_name]
    print("Collection created........")
    
    collection.create_index([ ("url", pymongo.ASCENDING) ], unique = True)
    return client, db, collection

#Initialise
client, db, collection = create_mongodb(databasename='news_scrap_db2', collection_name = 'news2')




# #GET TOKENS 
db = firestore.client()
users_ref = db.collection(u'users')

def get_token_all():
    docs = users_ref.stream()

    tokens_list = set()
    #tokens_list = []
    for doc in docs:
        try:
            tokens_list.add(doc.to_dict()["token"])
            #tokens_list.append(doc.to_dict()["token"])
        except Exception as E:
            print("For user", doc.to_dict()["uid"] )
            print(E)
    return list(tokens_list)



def get_top_news(time_gap):
    client, db, collection = create_mongodb(databasename='news_scrap_db2', collection_name = 'news2')

    today_time = time.time()

    old = collection.find(
        {"similarity_flag": 1, "summarized_flag": 1, "scrap_time": {"$gte": today_time - time_gap, "$lte":today_time   }},
        {"_id": 1,"title":1, "img_src" : 1, "primary_summary":1},
        sort = [("views_count", -1)],
        limit = 1
        )
    old = old[0]

    return old





#for single token

def send_to_token(trending_news):
    # [START send_to_token]
    # This registration token comes from the client FCM SDKs.
    registration_token = "eut81_eJQ1iHMhiEU5r-4i:APA91bH1n7WjV4MOO8s9qO1Aimc69P_mEnc0NYVkUF9J8CJAm-4RpfF5lXyiq96oybICwmXQo1BD0pOoeOUWzvWfXK6Wtos_mvJzW931nEP3eIOyXyYJIEuyCV8R0pqTPtU_7Q3Na5d0"


    title = "Trending..."
    message = trending_news["title"]
    ntf_data = {
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
        "view": "news_details",
        "news_id": str(trending_news["_id"])
        }
    fcm_token = registration_token
    #topic = "your_topic"

    # apns
    alert = messaging.ApsAlert(title = title, body = message)
    aps = messaging.Aps(alert = alert, sound = "default")
    payload = messaging.APNSPayload(aps)


    # See documentation on defining a message payload.
    message = messaging.Message(
        notification = messaging.Notification(
            title = title,
            body = message
        ),
        data = ntf_data,
        token = fcm_token,
        #topic = topic,
        apns = messaging.APNSConfig(payload = payload)
    )

    # Send a message to the device corresponding to the provided
    # registration token.
    response = messaging.send(message)
    # Response is a message ID string.
    print('Successfully sent message:', response)
    # [END send_to_token]



def android_message(trending_news):
    # [START android_message]
    message = messaging.Message(
        android=messaging.AndroidConfig(
            ttl=datetime.timedelta(seconds=3600), 
            priority='normal',
            notification=messaging.AndroidNotification(
                title='$GOOG up 1.43% on the day',
                body='$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.',
                color='#f45342'
            ),
        )
    )
    # [END android_message]
    return message


def send_multicast( trending_news, tokens_list):


    title = "Trending..."
    message = trending_news["title"].encode('ascii', 'ignore').decode('ascii')                #.encode('utf-8')
    image_link = trending_news["img_src"].encode('ascii', 'ignore').decode('ascii')           #.encode('utf-8')
    ntf_data = {
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
        "view": "news_details",
        "news_id": str(trending_news["_id"])
        }
    # fcm_token = registration_token
    #topic = "your_topic"

    # apns
    alert = messaging.ApsAlert(title = title, body = message)
    aps = messaging.Aps(alert = alert, sound = "default")
    payload = messaging.APNSPayload(aps)


    registration_tokens = tokens_list

    message = messaging.MulticastMessage(
        notification = messaging.Notification(
            title = title,
            body = message,
            image = image_link
        ),
        data = ntf_data,
        tokens = registration_tokens,
        #topic = topic,
        apns = messaging.APNSConfig(payload = payload)
    )
    response = messaging.send_multicast(message)
    # See the BatchResponse reference documentation
    # for the contents of response.
    print('{0} messages were sent successfully'.format(response.success_count))
    # [END send_multicast]



def job():
    try:
        tokens_list = get_token_all()

        time_gap = 60*60*3 #4hours
        trending_news = get_top_news(time_gap)
        #print(type(trending_news))
        print(trending_news["_id"])

        now = datetime.datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print("Time =", current_time)

        print("Total token used:", len(tokens_list))
        send_multicast(trending_news,tokens_list) #[registration_token]) 
    except Exception as E:
        now = datetime.datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print("Time =", current_time)
        print("Error:", E)



# print("Scheduling")
# schedule.every().day.at("04:30").do(job)
# schedule.every().day.at("07:30").do(job)
# schedule.every().day.at("10:30").do(job)
# schedule.every().day.at("13:30").do(job)
# schedule.every().day.at("16:30").do(job)
# schedule.every().day.at("19:00").do(job)





# while True:
#     schedule.run_pending()
#     time.sleep(1)

def send_to_topic(trending_news, topic_name):
    # [START send_to_topic]
    # The topic name can be optionally prefixed with "/topics/".
    topic = topic_name
    title = trending_news["title"].encode('ascii', 'ignore').decode('ascii')                #.encode('utf-8')
    message = trending_news["primary_summary"].encode('ascii', 'ignore').decode('ascii')
    image_link = trending_news["img_src"].encode('ascii', 'ignore').decode('ascii')           #.encode('utf-8')
    ntf_data = {
        "click_action": "FLUTTER_NOTIFICATION_CLICK",
        "view": "news_details",
        "news_id": str(trending_news["_id"]),
        "image": image_link
        }    

    alert = messaging.ApsAlert(title = title, body = message)
    aps = messaging.Aps(alert = alert, sound = "default")
    payload = messaging.APNSPayload(aps)
    # See documentation on defining a message payload.
    message = messaging.Message(
        notification = messaging.Notification(
            title = title,
            body = message,
            image = image_link
        ),
        data = ntf_data,
        topic = topic,
        apns = messaging.APNSConfig(payload = payload)
    )

    # Send a message to the devices subscribed to the provided topic.
    response = messaging.send(message)
    # Response is a message ID string.
    print('Successfully sent message:', response)
    # [END send_to_topic]



def job_topic(topic_name):
    try:
        #tokens_list = get_token_all()

        if topic_name == 'low':
            time_gap = 60*60*4 #4hours
        elif topic_name == 'mid':
            time_gap = 60*60*2 #4hours
        elif topic_name == 'high':
            time_gap = int(60*60*(1)) #4hours

        trending_news = get_top_news(time_gap)
        print("News Id:",trending_news["_id"])

        now = datetime.datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print("Time =", current_time)

        #print("Total token used:", len(tokens_list))

        send_to_topic(trending_news,topic_name) #[registration_token]) 
    except Exception as E:
        now = datetime.datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print("Time =", current_time)
        print("Error:", E)



#job_topic('high')

low_time = ["04:30", "08:30", "12:30", "16:30", "18:30"]
mid_time = ["04:30", "06:30", "08:30", "10:30", "12:30", "14:30", "16:30", "18:30"]
high_time =["04:30", "05:30", "06:30", "07:30", "08:30", "09:30", "10:30", "11:30", "12:30", "13:30", "14:30", "15:30", "16:30", "18:30"]

# low_time = ["08:00", "12:00", "16:00", "20:00", "16:53", "16:54"]
# mid_time = ["08:00", "10:00", "12:00", "14:00", "16:00", "18:00", "20:00", "16:59"]
# high_time =["08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00"]




for t in low_time:
    schedule.every().day.at(t).do(job_topic, topic_name = 'low')

for t in mid_time:
    schedule.every().day.at(t).do(job_topic, topic_name = 'mid')

for t in high_time:
    schedule.every().day.at(t).do(job_topic, topic_name = 'high')
    
#send_to_topic(trending_news, topic_name)

print(schedule.next_run())

while True:
    schedule.run_pending()
    time.sleep(1)




# def subscribe_to_topic():
#     topic = 'mid'
#     # [START subscribe]
#     # These registration tokens come from the client FCM SDKs.
#     registration_tokens = [token_temp]

#     # Subscribe the devices corresponding to the registration tokens to the
#     # topic.
#     response = messaging.subscribe_to_topic(registration_tokens, topic)
#     # See the TopicManagementResponse reference documentation
#     # for the contents of response.
#     print(response.success_count, 'tokens were subscribed successfully')
#     # [END subscribe]

# #subscribe_to_topic()