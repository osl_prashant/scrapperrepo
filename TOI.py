from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.error import HTTPError, URLError
import re


def scrapper_toi(url):
  #Loading url and html
  try:
    html = urlopen(url)
  except HTTPError as e:
    print(e)
    return {'url': url,
            'content': None}
  except URLError as e:
    print('The server could not be found!')
    return {'url': url,
            'content': None}
  else:
    print('Link Worked!')


  # Extracting Authour, Location, Date and Image src
  try:
    bs = BeautifulSoup(html.read(), 'html.parser')

    #remove script or style or both
    for script in bs(["script"]):
      script.decompose()
    try:
      editor = [f.strip() for f in bs.find('div', {'class' : 'byline-content'}).get_text().split('|')]
    except:
      print('Error in Editor extraction')
      editor = None

    time =  editor[-1].strip() 
  except:
    print('Error in time,author extraction')
    time = None

  # News Content
  try:
    content = bs.find('arttextxml').get_text().strip()
  except:
    return {'url': url,
            'content': None}

  # Title
  try:
    title = bs.find('arttitle').get_text().strip()
  except:
    print('Error in Title extraction')
    title = None

  try:
    location = content[:content.find(':')]
    if len(location) > 18:
      print('Too long location')
      location = None
  except:
    print('Error in extraction location')
    location = None

  #Image
  img_src = None
  
  return {
      'url': url,
      'news_source': 'The Times of India',
      #'author': author,
      'location': location,
      'img_src': img_src,
      'time': time,
      'title': title,
      'editor': editor,
      'content': content
  }
  
    

def latest_news_urls_toi(url_latest):
  #Loading url and html
  try:
    html = urlopen(url_latest)
  except HTTPError as e:
    print(e)
    return None
  except URLError as e:
    print('The server could not be found!')
    return None
  else:
    print('Link Worked! Latest Page extraction')


  # Extracting Links of Articles
  try:
    bs = BeautifulSoup(html.read(), 'html.parser')

    # remove script or style or both
    for script in bs(["script"]):
      script.decompose()


    image_list = bs.find_all('a', class_ = 'w_img') #image contatins all important information

    articles_url = [ {
        'url': image.attrs['href'],
        'title': image.attrs['title'],
        'img_src': image.img.attrs['data-src']
     } for image in image_list ]
    
  except:
    print('Error in Extracting Links of Articles from Image.')
    return None

  
  return articles_url


    




def scrap_latest_toi(url_latest = 'https://timesofindia.indiatimes.com/news'):
  news_url_list = latest_news_urls_toi(url_latest)

  scrapped_data = []
  for news in news_url_list:
    dict_scrapped = scrapper_toi('https://timesofindia.indiatimes.com' + news['url'])
    dict_scrapped['img_src'] = news['img_src']
    scrapped_data.append(dict_scrapped)
  
  return scrapped_data
    


    

#print(scrap_latest_toi())