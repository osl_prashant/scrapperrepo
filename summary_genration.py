import requests
import pymongo
from pymongo import MongoClient
import time

def create_mongodb(databasename='news_scrap_db', collection_name = 'news'):
    #Creating a pymongo client
    client = MongoClient("mongodb://user1:user1password@139.99.45.55/")#MongoClient('localhost', 27017)

    #Getting the database instance
    db = client[databasename]
    print("Database created........")
    print(client.list_database_names())
    
    collection = db[collection_name]
    print("Collection created........")
    

    collection.create_index([ ("url", pymongo.ASCENDING) ], unique = True)
    return client, db, collection


client, db, collection = create_mongodb(databasename='news_scrap_db', collection_name = 'news') 
client, db, collection_write = create_mongodb(databasename='news_scrap_db', collection_name = 'news_summary')



# summary API
uri = 'http://139.99.45.55:5000'
endpoint_home = '/home'
endpoint_primary_summary = '/primary_summary'
endpoint_secondary_summary = '/secondary_summary'
endpoint_keywords_spacy = '/keywords_spacy'
endpoint_keywords_textrank = '/keywords_textrank'



r = requests.get(url = uri + endpoint_home) 
print(r)
print(r.json() )

while True:

    fresh_scrapped_data = collection.find({"flag":{ "$exists": False }})
    #print('New News articles',len(fresh_scrapped_data))

    for coll in fresh_scrapped_data:

        
        collection.find_one_and_update( {"_id":coll['_id']}, {'$set': {'flag': True}} )


        try:
            #scraped_data = collection.find_one( { "secondary_summary" : { "$exists": False } } )
            scraped_data = coll

            
            summary_data = scraped_data #copy to modify
            if  scraped_data['content'] is not None :

                #print(scraped_data['title'])

                #secondary_summary
                try:
                    body = {
                        "news": scraped_data['content'],
                        "split": "True",
                        "ratio": 0.25,
                        "word_count":"None"
                        }
                    r = requests.post(url = uri + endpoint_secondary_summary, json = body) 
                    #print(r.json())
                    summary_data['secondary_summary'] = r.json()['Secondary_Summary']
                except:
                    print('Error in Secondary Summary')
                    summary_data['secondary_summary'] = None

                #primary_summary
                try:
                    body = {
                        "news": scraped_data['content'],
                        "min_len": 90,
                        "max_len":120
                        }
                    
                    r = requests.post(url = uri + endpoint_primary_summary, json = body) 
                    #print(r.json())
                    summary_data['primary_summary'] = r.json()['Primary_Summary']
                except:
                    print('Error in Primary Summary')
                    summary_data['primary_summary'] = None

                #keywords_spacy
                try:
                    body = {
                        "news": scraped_data['content'],
                        }
                    
                    r = requests.post(url = uri + endpoint_keywords_spacy, json = body) 
                    #print(r.json())
                    summary_data['keywords_spacy'] = r.json()['keywords_spacy']
                except:
                    print('Error in keywords_spacy')
                    summary_data['keywords_spacy'] = None
                    
                #keywords_textrank
                try:

                    body = {
                        "news": scraped_data['content'],
                        "split":"True",
                        "ratio": "None",
                        "keyword_count":12,
                        "scores": "True"
                        }
                    
                    r = requests.post(url = uri + endpoint_keywords_textrank, json = body) 
                    #print(r.json())
                    summary_data['keywords_textrank'] = r.json()['keywords_gensim']
                except:
                    print('Error in keywords_textrank')
                    summary_data['keywords_textrank'] = None

                collection_write.insert_one(summary_data)
            else:
                print("Scrapped Data is None")
            
        except:
            print('Error in this URL',scraped_data['url'] )


    print('Sleeping for 5 minutes')
    time.sleep(600)



#primary_data
#print(scraped_data)
#collection_write.repl


#collection.find_one_and_replace(scraped_data,summary_data )



#r = requests.post(url = url, json = data) 

#