from IndianExpress import scrap_latest_indian_express
from TOI import scrap_latest_toi
import pymongo
from pymongo import MongoClient
import time

def create_mongodb(databasename='news_scrap_db', collection_name = 'news'):
    #Creating a pymongo client
    client = MongoClient("mongodb://user1:user1password@139.99.45.55/")#MongoClient('localhost', 27017)

    #Getting the database instance
    db = client[databasename]
    print("Database created........")
    print(client.list_database_names())
    
    collection = db[collection_name]
    print("Collection created........")
    
    collection.create_index([ ("url", pymongo.ASCENDING) ], unique = True)
    return client, db, collection


client, db, collection = create_mongodb(databasename='news_scrap_db', collection_name = 'news')

while True:

    try:
        scrapped_data_IndianExpress = scrap_latest_indian_express()
        #scrapped_data_TOI = scrap_latest_toi()



        for n in scrapped_data_IndianExpress:
            try:
                collection.insert_one(n)
            except :
                print('Duplicate')
    except:
        print('Error in scrap_latest_****')

    
    time.sleep(600)

