from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.error import HTTPError, URLError
import re

def scrapper_indian_express(url):
  #Loading url and html
  #print(url)
  try:
    html = urlopen(url)
  except HTTPError as e:
    print(e)
    return {'url': url,
            'content': None}
  except URLError as e:
    print('The server could not be found!')
    return {'url': url,
            'content': None}
  else:
    print('Link Worked!')


  # Extracting Authour, Location, Date and Image src
  try:
    bs = BeautifulSoup(html.read(), 'html.parser')

    #remove script or style or both
    for script in bs(["script"]):
      script.decompose()
    try:
      editor = [f.strip() for f in bs.div.find(class_='editor').get_text().split('|')]
    except:
      print('Error in Editor extraction')
      editor = None
    
    author = editor[0][editor[0].find(':')+1:].strip()
    location = editor[1].strip()
    time = editor[2].strip()
    
  except:
    print('Error in author,location, time Extraction.')
    author,location,time = None,None, None
    
    

  # Image Extraction  
  try:
    img_src = bs.find('img', class_ = re.compile('wp-image.')).attrs['data-lazy-src']
  except:
    print('Error in Image Extraction')
    img_src = None

  # Title Extraction
  try:
    title = bs.find(class_='native_story_title').get_text().strip()
  except:
    print('Error in Title Extraction.')
    title = None

  # Content Extraction
  news = []
  try: 
    for para in bs.find(class_ = 'articles').find_all('p',attrs={'class':''}):
      news.append(para.get_text())
    
    content = ' '.join(news)
  except:
    print('Error in News Extraction.')
    return {'url': url,
            'content': None}

  return {
      'url': url,
      'news_source': 'Indian Express',
      'author': author,
      'location': location,
      'img_src': img_src,
      'time': time,
      'title': title,
      'editor': editor,
      'content': content
  }


def latest_news_urls_indian_express(url_latest):
  #Loading url and html
  try:
    html = urlopen(url_latest)
  except HTTPError as e:
    print(e)
    return None
  except URLError as e:
    print('The server could not be found!')
    return None
  else:
    print('Link Worked!')


  # Extracting Links of Articles
  try:
    bs = BeautifulSoup(html.read(), 'html.parser')

    # remove script or style or both
    for script in bs(["script"]):
      script.decompose()

    articles_list = bs.find('div',class_='leftpanel').find_all('div', class_ = 'title')
    articles_url = [ 
                    {
                        'url':a.find('a').attrs['href'], 
                        'title':a.find('a').get_text()
                     } for a in articles_list ]
    
  except:
    print('Error in Extracting Links of Articles.')
    return None

  
  return articles_url



def scrap_latest_indian_express(url_latest = 'https://indianexpress.com/latest-news/'):
  news_url_list = latest_news_urls_indian_express(url_latest)

  scrapped_data = []
  for news in news_url_list:
    dict_scrapped = scrapper_indian_express(news['url'])
    if dict_scrapped['content'] is not None:
        scrapped_data.append(dict_scrapped)


  return scrapped_data